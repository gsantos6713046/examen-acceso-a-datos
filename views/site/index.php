<?php
use yii\helpers\Html;
/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

<div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas Del Exámen Práctico</h1>
    </div>
    <div class="row">
            <!--
            botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p>Los ciclistas cuya edad está entre 20 y 40 años mostrando únicamente el dorsal y el nombre del ciclista</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- 
            fin botón de consulta 1
            -->
            <!--
            botón de consulta 2
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p>Las etapas circulares mostrando sólo el número de etapa y la longitud de las mismas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- 
            fin botón de consulta 2
            -->
            <!--
            botón de consulta 3
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 3</h3>
                        <p>Los puertos con altura mayor a 1000 metros figurando el nombre del puerto y el dorsal del ciclista que lo ganó</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- 
            fin botón de consulta 3
            -->
        </div>
</div>
